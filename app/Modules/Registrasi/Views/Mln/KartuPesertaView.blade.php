<link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet'>
<style>
body {
    font-family: 'Asap';font-size: 22px;
}
</style>

<table width="585" height="80"  border="0" style="font-size:12px; line-height: 1.4">
  <tr>
    <td width="12%" valign="top" ALIGN="right"><img src="{{asset('public/images/persistarogonglogo_flat.png')}}" height="68" /></td>
    <td width="37%" align="center" valign="middle"><b>PENERIMAAN SANTRI BARU<br>MU'ALLIMIN/ MA PERSIS TAROGONG<br>Tahun Pelajaran 2019-2020</b></td>
   <td width="5%" rowspan="5" valign="top"></td>
   <td width="46%" align="center" valign="bottom"><b> Informasi Seleksi Penerimaan Santri Baru<br>
    </b><b>MU’ALLIMIN/ MA PERSIS TAROGONG</b>
    <left></left></td>
  </tr>
</table>

<table width="585" height="300" border="0" style="font-size:10px; line-height: 1.4">
  <tr>
    <td width="49%" colspan="4" align="center" valign="middle" bgcolor="#007599">&nbsp;</td>
    <td width="5%" rowspan="9" valign="top"></td>
    <td width="46%" height="20" colspan="3"s align="center" bgcolor="#007599">
      <span style="color:#FFF"><b>HARI 1 - SABTU, 23 FEBRUARI 2019</b></span>
    </td>
  </tr>
  <tr>
    <td colspan="4" rowspan="2" align="center" valign="middle">Nama Peserta<br>
        <b style="font-size:14px; padding-left:10; padding-right:10">{{$dataCalonSantri->nama_lengkap}}</b></td>
    <td width="16%" height="75" valign="top"><div align="center"><strong>WAKTU</strong></div>
    <div align="center">07.00 - 07.30<br>
	07.30 - 10.00<br>

      10.00 - 10.30<br>
      10.30 - 12.00<br>
    </div>
    </td>
    <td width="30%" valign="top"><div align="LEFT"><strong>KEGIATAN</strong></div>
    <div align="left">Pengarahan<br>
          <strong>TES TULIS/ AKADEMIK</strong><br>
          Istirahat          <br>
        <strong>TES Tilawah &amp; Hifzh Al-Quran (THQ)</strong> <br>
    </div></td>
  </tr>
  <tr>
    <td height="5" colspan="3" align="center"></td>
  </tr>
  <tr>
    <td width="14%" rowspan="2" valign="top" style="font-size:11px; padding-left:8">No Peserta</td>
    <td width="2%" rowspan="2" valign="top" style="font-size:11px;">:</td>
    <td width="16%" rowspan="2" valign="top" style="font-size:11px;"><b>{{$dataCalonSantri->no_tes}}</b></td>
    <td width="17%" rowspan="2" valign="top" style="font-size:11px;">Ruang : <b>{{$dataCalonSantri->no_ruang}}</b></td>
    <td height="20" colspan="3" align="center" bgcolor="#007599"><span style="color:#FFF"><b>HARI 2 - AHAD, 24 FEBRUARI 2019</b></span></td>
  </tr>
  <tr>
    <td height="90" rowspan="3" valign="top"><div align="center"><strong>WAKTU</strong></div>
    <div align="center">07.00 - 07.30	<br>
      07.30 - 11.30<br>
      11.30 - 12.30<br>
      <br>
    </div>
    </td>
    <td rowspan="3" valign="top"><div align="left"><strong>KEGIATAN</strong></div>
    <div align="left"><strong>Angket Kesiapan<br>
      PSIKOTES</strong> 
          <br>
        <strong>WAWANCARA</strong> (beasiswa)<br>
    </span></div></td>
  </tr>
  <tr>
    <td height="15" valign="top" style="font-size:11px; padding-left:8" margin-left="10">Asal Sekolah</td>
    <td valign="top" style="font-size:11px;">: </td>
    <td colspan="2" valign="top" style="font-size:11px;">{{$dataCalonSantri->sekolah_asal}}</td>
  </tr>
  <tr>
    <td height="15" valign="top" style="font-size:11px; padding-left:8; line-height:1.2" margin-left="10">Alamat</td>
    <td valign="top" style="font-size:11px;">: </td>
    <td colspan="2" valign="top" style="font-size:11px; line-height:1.2">{{$dataCalonSantri->alamat_lengkap}} 
      {{$dataCalonSantri->kelurahan}} 
      {{$dataCalonSantri->kecamatan}} 
      {{$dataCalonSantri->kabupaten}}
    {{$dataCalonSantri->provinsi}} </td>
  </tr>

  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td width="15%" align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle">Garut, {{$date}}<br>
      <br>
    Panitia</td>
    <td height="60" colspan="3" rowspan="2" valign="middle" align="left" bgcolor="#CCCCCC" style="padding-left:20; padding-right:10">
        <span style="font-size:9px;">KETERANGAN:<br>
            - Kartu peserta WAJIB dibawa pada saat Tes/Seleksi<br>
            - Alat tulis untuk tes disediakan oleh panitia (GRATIS), <br>&nbsp; berupa: pensil 2B, penghapus, dan papan ujian<br>
            - Peserta diharapkan hadir 15 menit sebelum mulai</span></td>
  </tr>
  <tr>
    <td height="25" colspan="4" valign="middle" bgcolor="#007599"><div style="color:#FFF; font-size:10px" align="center"><strong>PESANTREN PERSATUAN ISLAM TAROGONG </strong></div></td>
  </tr>
</table>


<br>
<script>
  window.print();
</script>