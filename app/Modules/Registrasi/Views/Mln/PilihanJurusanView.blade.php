{{-- @extends('Layouts::layout')
@section('content') --}}
    <br>
    <h5>Pilihan Peminatan / Jurusan</h5>
    <hr>
    <div class="row">
    	<form action="{{url('/registrasi/mln/update/jurusan')}}" method="POST" role="form" enctype="multipart/form-data" class="col-md-8">
            {{ csrf_field() }}
            @if($dataCalonSantri)
            <input type="hidden" name="id" value="{{$dataCalonSantri->id}}"/>
            @endif 
            <div class="form-group row">
                <label class="col-md-4 col-form-label col-form-label-sm">Pilihan 1</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                             <select name="pilihan_jurusan_1" class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_1}}">
                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_1}}
                               </option>
                                <option value='IAI'>IAI</option>
                                <option value='IPA'>IPA</option>
                                <option value='IPS'>IPS</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label col-form-label-sm">Pilihan 2</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                             <select name="pilihan_jurusan_2" class="form-control form-control-sm" required>
                               <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_2}}">
                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_2}}
                               </option>
                                <option value='IAI'>IAI</option>
                                <option value='IPA'>IPA</option>
                                <option value='IPS'>IPS</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label col-form-label-sm">Pilihan 3</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                             <select name="pilihan_jurusan_3" class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_3}}">
                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_3}}
                               </option>
                                <option value='IAI'>IAI</option>
                                <option value='IPA'>IPA</option>
                                <option value='IPS'>IPS</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label col-form-label-sm">Bersedia ditempatkan di jurusan manapun berdasarkan pertimbangan pesantren? </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="pilihan_pesantren" id="" value="1" checked>
                                Ya
                              </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="pilihan_pesantren" id="" value="0" checked>
                                Tidak
                              </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary float-right">Submit</button>
        </form>
    </div>
{{-- @endsection --}}