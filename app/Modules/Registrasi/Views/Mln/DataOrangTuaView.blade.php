{{-- @extends('Layouts::layout')
@section('content') --}}
    <br>
    <h5>Data Orang Tua / Wali Santri</h5>
    <hr>
    <div class="row">
    	<form action="{{url('/registrasi/mln/update/orang-tua')}}" method="POST" role="form" enctype="multipart/form-data" class="col-md-8">
            {{ csrf_field() }}
            @if($dataCalonSantri)
            <input type="hidden" name="id" value="{{$dataCalonSantri->id}}"/>
            @endif 
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Data Ayah Kandung</label>
                <div class="col-md-9">
                    <input 
                        type="text" 
                        name="nama_ayah" 
                        class="form-control form-control-sm" 
                        placeholder="Nama Lengkap" 
                        value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nama_ayah}}"
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm"></label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                             <select name="pendidikan_ayah" class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pendidikan_ayah}}">
                                    {{!$dataCalonSantri ? 'Pendidikan' : $dataCalonSantri->pendidikan_ayah}}
                                </option>
                                <option value="SD">SD</option>
                                <option value="SMP">SMP</option>
                                <option value="SMA">SMA</option>
                                <option value="D1/D2">D1/D2</option>
                                <option value="D3/D4">D3/D4</option>
                                <option value="S1">S1</option>
                                <option value="S2">S2</option>
                                <option value="S3">S3</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select name="pekerjaan_ayah" class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pekerjaan_ayah}}">
                                    {{!$dataCalonSantri ? 'Pekerjaan' : $dataCalonSantri->pekerjaan_ayah}}
                                </option>
                                <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                                <option value="Guru/Dosen/Pengajar">Guru/Dosen/Pengajar</option>
                                <option value="Petani">Petani</option>
                                <option value="Buruh">Buruh</option>
                                <option value="Pensiunan">Pensiunan</option>
                                <option value="Tentara/Polisi">Tentara/Polisi</option>
                                <option value="Karyawan Swasta">Karyawan Swasta</option>
                                <option value="Wiraswasta">Wiraswasta</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Data Ibu Kandung</label>
                <div class="col-md-9">
                    <input 
                        type="text" 
                        name="nama_ibu" 
                        class="form-control form-control-sm" 
                        placeholder="Nama lengkap" 
                        value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nama_ibu}}"
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm"></label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                             <select name="pendidikan_ibu" class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pendidikan_ibu}}">
                                    {{!$dataCalonSantri ? 'Pendidikan' : $dataCalonSantri->pendidikan_ibu}}
                                </option>
                                <option value="SD">SD</option>
                                <option value="SMP">SMP</option>
                                <option value="SMA">SMA</option>
                                <option value="D1/D2">D1/D2</option>
                                <option value="D3/D4">D3/D4</option>
                                <option value="S1">S1</option>
                                <option value="S2">S2</option>
                                <option value="S3">S3</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select name="pekerjaan_ibu" class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pekerjaan_ibu}}">
                                    {{!$dataCalonSantri ? 'Pekerjaan' : $dataCalonSantri->pekerjaan_ibu}}
                                </option>
                                <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                                <option value="Guru/Dosen/Pengajar">Guru/Dosen/Pengajar</option>
                                <option value="Petani">Petani</option>
                                <option value="Buruh">Buruh</option>
                                <option value="Pensiunan">Pensiunan</option>
                                <option value="Tentara/Polisi">Tentara/Polisi</option>
                                <option value="Karyawan Swasta">Karyawan Swasta</option>
                                <option value="Wiraswasta">Wiraswasta</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Data Wali (Jika ada)</label>
                <div class="col-md-9">
                    <input 
                        type="text" 
                        name="nama_wali" 
                        class="form-control form-control-sm" 
                        placeholder="Nama lengkap" 
                        value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nama_wali}}"
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm"></label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                             <select name="pendidikan_wali" class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pendidikan_wali}}">
                                    {{!$dataCalonSantri ? 'Pendidikan' : $dataCalonSantri->pendidikan_wali}}
                                </option>
                                <option value="SD">SD</option>
                                <option value="SMP">SMP</option>
                                <option value="SMA">SMA</option>
                                <option value="D1/D2">D1/D2</option>
                                <option value="D3/D4">D3/D4</option>
                                <option value="S1">S1</option>
                                <option value="S2">S2</option>
                                <option value="S3">S3</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select name="pekerjaan_wali" class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pekerjaan_wali}}">
                                    {{!$dataCalonSantri ? 'Pekerjaan' : $dataCalonSantri->pekerjaan_wali}}
                                </option>
                                <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                                <option value="Guru/Dosen/Pengajar">Guru/Dosen/Pengajar</option>
                                <option value="Petani">Petani</option>
                                <option value="Buruh">Buruh</option>
                                <option value="Pensiunan">Pensiunan</option>
                                <option value="Tentara/Polisi">Tentara/Polisi</option>
                                <option value="Karyawan Swasta">Karyawan Swasta</option>
                                <option value="Wiraswasta">Wiraswasta</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Penghasilan Orang Tua / Wali</label>
                <div class="col-md-9">
                    <select class="form-control form-control-sm" name="penghasilan_orangtua" required>
                        <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->penghasilan_orangtua}}">
                            {{!$dataCalonSantri ? '' : $dataCalonSantri->penghasilan_orangtua}}
                        </option>
                        <option value='< Rp 500.000'>< Rp 500.000</option>
                        <option value='Rp 500.001 - Rp 1.000.000'>Rp 500.001 - Rp 1.000.000</option>
                        <option value='Rp 1.000.001 - Rp 2.000.000'>Rp 1.000.001 - Rp 2.000.000</option>
                        <option value='Rp 2.000.001 - Rp 3.000.000'>Rp 2.000.001 - Rp 3.000.000</option>
                        <option value='Rp 3.000.001 - Rp 5.000.000'>Rp 3.000.001 - Rp 5.000.000</option>
                        <option value='> Rp 5.000.000'>> Rp 5.000.000</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Penanggung Biaya Pendidikan</label>
                <div class="col-md-9">
                    <select class="form-control form-control-sm" name="penanggung_biaya" required>
                        <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->penanggung_biaya}}">
                            {{!$dataCalonSantri ? '' : $dataCalonSantri->penanggung_biaya}}
                        </option>
                        <option value='Orang Tua'>Orang Tua</option>
                        <option value='Saudara'>Saudara</option>
                        <option value='Wali'>Wali</option>
                        <option value='Beasiswa'>Beasiswa</option>
                        <option value='Lainnya'>Lainnya</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Hubungan Wali dengan Anak</label>
                <div class="col-md-9">
                    <input 
                        type="text" 
                        name="hubungan_wali_anak" 
                        class="form-control form-control-sm" 
                        placeholder="" 
                        value="{{!$dataCalonSantri ? '' : $dataCalonSantri->hubungan_wali_anak}}"
                        required>
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Alamat Lengkap</label>
                <div class="col-md-9">
                    <textarea 
                        type="text" 
                        name="alamat_lengkap" 
                        class="form-control form-control-sm"
                        required>{{!$dataCalonSantri ? '' : $dataCalonSantri->alamat_lengkap}}
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Provinsi</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <select name="provinceId" class="form-control form-control-sm" required>
                               <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->provinsi}}">
                                    {{!$dataCalonSantri ? 'Silahkan pilih provinsi' : $dataCalonSantri->provinsi}}
                               </option>
                               @foreach($provinces as $province)
                                <option data-id="{{$province->id}}" value='{{$province->name}}'>{{$province->name}}</option>
                               @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Kabupaten / Kota</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <select name='cityId' class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->kabupaten}}">
                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->kabupaten}}
                                </option>
                            </select>
                            @if ($errors->all())
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('cityId') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Kecamatan</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <select name='districtId' class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->kecamatan}}">
                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->kecamatan}}
                                </option>
                            </select>
                            @if ($errors->all())
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('districtId') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Kelurahan / Desa</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <select name="villageId" class="form-control form-control-sm" required>
                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->kelurahan}}">
                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->kelurahan}}
                                </option>
                            </select>
                            @if ($errors->all())
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('villageId') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>           
            <hr>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">No HP 1</label>
                <div class="col-md-9">
                    <input 
                        type="number" 
                        name="no_hp_1" 
                        class="form-control form-control-sm" 
                        placeholder="No HP" 
                        value="{{!$dataCalonSantri ? '' : $dataCalonSantri->no_hp_1}}"
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">No HP 2</label>
                <div class="col-md-9">
                    <input 
                        type="number" 
                        name="no_hp_2" 
                        class="form-control form-control-sm" 
                        placeholder="No HP" 
                        value="{{!$dataCalonSantri ? '' : $dataCalonSantri->no_hp_2}}"
                        required>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary float-right">Submit</button>
        </form>
    </div>
{{-- @endsection --}}