
    <br>
    <h5>Pembayaran</h5>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Pembayaran via Bank Transfer
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <strong>Silahkan transfer biaya sebesar <b class="text-success">Rp300.000.-</b> ke rekening pembayaran berikut:</strong>
                            <br><br>
                            <ol class="pl-3">
                                <li>Bank: BCA <br> No Rekening: 12434546 <br> A/n: Fulan</li>
                                <li>Bank: BCA <br> No Rekening: 12434546 <br> A/n: Fulan</li>
                                <li>Bank: BCA <br> No Rekening: 12434546 <br> A/n: Fulan</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Pembayaran via Virtual Account BNI Syariah
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            <ol>
                                <li>Anim pariatur cliche reprehenderit</li>
                                <li>Anim parcliche reprehenderit</li>
                                <li>Anim priatur cliche reprehenderit</li>
                            </ol>    
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="col-md-6">
                <div class="col-md-12">
                    <strong>Konfirmasi Pembayaran via Bank Transfer</strong><br><br>
                </div>
                <form action={{url('/registrasi/mln/konfirmasi-pembayaran')}}  
                    method="POST" 
                    role="form" 
                    enctype="multipart/form-data" 
                    class="col-md-12">
                        @if($dataCalonSantri)
                        <input type="hidden" name="id" value="{{$dataCalonSantri->id}}"/>
                        @endif 
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Bank Pengirim</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    name="bank_pengirim" 
                                    class="form-control form-control-sm" 
                                    placeholder="Bank Pengirim" 
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Nama Akun Pengirim</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    name="nama_pengirim" 
                                    class="form-control form-control-sm" 
                                    placeholder="Nama Akun Pengirim" 
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">No Rekening Pengirim</label>
                            <div class="col-md-9">
                                <input 
                                    type="number" 
                                    name="norek_pengirim" 
                                    class="form-control form-control-sm" 
                                    placeholder="No Rekening" 
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Bank Tujuan</label>
                            <div class="col-md-9">
                                <select class="form-control form-control-sm" name="bank_penerima" required>
                                    <option value="Bank 1">Bank 1</option>
                                    <option value="Bank 2">Bank 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Jumlah Transfer</label>
                            <div class="col-md-9">
                                <input 
                                    type="number" 
                                    name="jumlah_transfer" 
                                    class="form-control form-control-sm" 
                                    placeholder="Jumlah Transfer" 
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Tanggal Transfer</label>
                            <div class="col-md-9">
                                <input 
                                    type="date" 
                                    name="tanggal_transfer" 
                                    class="form-control form-control-sm" 
                                    placeholder="Tanggal Transfer" 
                                    required>
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary float-right">Konfirmasi Pembayaran</button>
                </form>
        </div>
    </div>
