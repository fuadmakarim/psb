<table border="0" width="1100" style="font-family:'Courier New', Courier, monospace; font-size:14px; letter-spacing:0.3em; font-variant:small-caps">
<tr>
  <td width="170" height="112" align="center"><img src="{{asset('public/images/persistarogonglogo_BW2.png')}}" width="160" height="160" alt="" /></td>
  <td height="112" colspan="3"><div align="center">
	<img src="{{asset('public/images/mahad.png')}}" width="850" />
    <div style="font-size:16px">JALAN TERUSAN PEMBANGUNAN NO.1 TAROGONG KIDUL GARUT 44151</div>
  </div></td>
</tr>
<tr>
  <td height="18" colspan="4" valign="top">
  <hr color="#000000">
  </td>
</tr>
<tr>
  <td height="20" colspan="4" valign="top">
  <div align="center"><br></div>
  </td>
  </tr>
<tr>
  <td height="30" colspan="4" valign="top">
  <div align="center" style="font-size:35px">TANDA BUKTI PEMBAYARAN<br></div>
  </td>
  </tr>
  <tr>
  <td height="40" colspan="4" valign="top">
  <div align="center"><br></div>
  </td>
  </tr>
<tr>
  <td height="30" colspan="2" valign="top" style="font-size:20px">ATAS NAMA</td>
  <td colspan="2" valign="top" style="font-size:20px">: 
    <b>{{$dataCalonSantri->nama_lengkap}}</b></td>
</tr>
<tr>
  <td height="30" colspan="2" valign="top" style="font-size:20px">SEBESAR</td>
  <td colspan="2" valign="top" style="font-size:20px">: Rp 350.000,-</td>
</tr>
<tr>
  <td height="30" colspan="2" valign="top" style="font-size:20px">TERBILANG </td>
  <td colspan="2" valign="top" style="font-size:20px">: TIGA RATUS LIMA PULUH RIBU RUPIAH</td>
</tr>

<tr>
  <td height="30" colspan="2" valign="top" style="font-size:20px">UNTUK PEMBAYARAN</td>
    <td colspan="2" valign="top" style="font-size:20px">: PENDAFTARAN SANTRI BARU MU'ALLIMIN 2019/2020</td>
</tr>
<tr>
  <td height="40" colspan="4"></td>
  </tr>

<tr>
  <td height="30" colspan="2" valign="top" style="font-size:20px"></td>
  <td valign="top" align="center">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</td>
  <td valign="top" align="center" style="font-size:20px">GARUT, {{$date}}</td>
</tr>

<tr>
  <td height="40" colspan="4"></td>
  </tr>

<tr>
  <td height="40" colspan="4" valign="top">
  <div align="center"><br></div>
  </td>
  </tr>
<tr>
  <td valign="top" align="center">&nbsp;</td>
  <td valign="top" align="center" style="font-size:20px">(PENYETOR)</td>
  <td valign="top" align="center">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</td>
  <td valign="top" align="center" style="font-size:20px">(PENERIMA)</td>
</tr>
</table>
<br>
<script>
  window.print();
</script>