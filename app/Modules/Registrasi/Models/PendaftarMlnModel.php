<?php

namespace App\Modules\Registrasi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class PendaftarMlnModel extends Model {
	protected $table = 'pendaftar_mln';

    public function __construct() {
        $this->table = 'pendaftar_mln';
    }

    public function getPendaftarOnline(){
        $pendaftar = DB::table('pendaftar_mln')
            ->where('is_admin', 0)
            ->get();

        return $pendaftar;
    }

    public function getPendaftarFinal(){
        $pendaftar = DB::table('pendaftar_mln')
            ->where('no_tes', '!=', null)
            ->get();

        return $pendaftar;
    }

    public function getCountPendaftarJk(){
        $pendaftar = DB::table('pendaftar_mln')
           ->select('jenis_kelamin', DB::raw('count(*) as total'))
           ->groupBy('jenis_kelamin')
           ->get();
        return $pendaftar;
    }

    public function getCountPendaftarJalurMasuk(){
        $pendaftar = DB::table('pendaftar_mln')
           ->select('jalur_masuk', DB::raw('count(*) as total'))
           ->groupBy('jalur_masuk')
           ->get();
        return $pendaftar;
    }

    public function getCountPendaftarSekolahAsal(){
        $pendaftar = DB::table('pendaftar_mln')
           ->select('kategori_sekolah_asal', DB::raw('count(*) as total'))
           ->groupBy('kategori_sekolah_asal')
           ->get();
        return $pendaftar;
    }

    public function getCountDataSantri($kolom){
        $pendaftar = DB::table('pendaftar_mln')
           ->select($kolom, DB::raw('count(*) as total'))
           ->groupBy($kolom)
           ->get();
        return $pendaftar;
    }
}