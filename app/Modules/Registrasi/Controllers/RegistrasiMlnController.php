<?php

namespace App\Modules\Registrasi\Controllers;
use Validator;
use Illuminate\Http\Request;
use Laravolt\Indonesia\Facade;
use App\Http\Controllers\Controller;
use App\Modules\Registrasi\Models\PendaftarMlnModel;
use App\Modules\Registrasi\Models\PembayaranMlnModel;
use App\Modules\Registrasi\Models\StatusRegistrasiModel;
use App\Modules\Registrasi\Models\StatusRuanganModel;
use Illuminate\Support\Facades\Auth;

class RegistrasiMlnController extends Controller
{
    //Data
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
    	return view('Registrasi::Mln.DataSantriView');
    }
    public function create(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'nisn' => 'required',
            'tahun_lulus' => 'required',
            'tempat_lahir' => 'required',
            'hari' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
            'jenis_kelamin' => 'required',
            'hobi' => 'hobi',
            'cita_cita' => 'cita_cita',
            'anak_ke' => 'required',
            'dari_bersaudara' => 'required',
            'sekolah_asal' => 'required',
            'alamat_sekolah_asal' => 'required',
            'kategori_sekolah_asal' => 'required',
            'rencana_tinggal' => 'required',
            'dorongan_masuk' => 'required',
            'sumber_informasi' => 'required'
        ]);
        $pendaftarMln = new PendaftarMlnModel;
        
        $hari = $request->input('hari');
        $bulan = $request->input('bulan');
        $tahun = $request->input('tahun');
        $tanggal_lahir = $tahun.'-'.$bulan.'-'.$hari;
        
        $pendaftarMln->is_admin = 0;
        $pendaftarMln->nama_lengkap = $request->input('nama_lengkap');
        $pendaftarMln->nisn = $request->input('nisn');
        $pendaftarMln->tahun_lulus = $request->input('tahun_lulus');
        $pendaftarMln->tempat_lahir = $request->input('tempat_lahir');
        $pendaftarMln->tanggal_lahir = $tanggal_lahir;
        $pendaftarMln->jenis_kelamin = $request->input('jenis_kelamin');
        $pendaftarMln->hobi = $request->input('hobi');
        $pendaftarMln->cita_cita = $request->input('cita_cita');
        $pendaftarMln->anak_ke = $request->input('anak_ke');
        $pendaftarMln->dari_bersaudara = $request->input('dari_bersaudara');
        $pendaftarMln->sekolah_asal = $request->input('sekolah_asal');
        $pendaftarMln->alamat_sekolah_asal = $request->input('alamat_sekolah_asal');
        $pendaftarMln->kategori_sekolah_asal = $request->input('kategori_sekolah_asal');
        $pendaftarMln->rencana_tinggal = $request->input('rencana_tinggal');
        $pendaftarMln->dorongan_masuk = $request->input('dorongan_masuk');
        $pendaftarMln->sumber_informasi = $request->input('sumber_informasi');
        $pendaftarMln->user_id = $userId;
        $pendaftarMln->jalur_masuk = "Reguler";
        $pendaftarMln->save();
        
        //input status
        $statusRegistrasi = new StatusRegistrasiModel;
        $statusRegistrasi->user_id = $userId;
        $statusRegistrasi->status = "data_santri";
        $statusRegistrasi->status_data_santri = 1;
        $statusRegistrasi->save();

        return redirect('/')->with('status', 'Data Calon Santri berhasil disimpan');

    }
    public function updateOrtu(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'nama_ayah' => 'required',
            'pendidikan_ayah' => 'required',
            'pekerjaan_ayah' => 'required',
            'nama_ibu' => 'required',
            'pendidikan_ibu' => 'required',
            'pekerjaan_ibu' => 'required',
            'penghasilan_orangtua' => 'required',
            'penanggung_biaya' => 'required',
            'alamat_lengkap' => 'required',
            'provinceId' => 'required',
            'cityId' => 'required',
            'districtId' => 'required',
            'villageId' => 'required',
            'no_hp_1' => 'required',
            'no_hp_2' => 'required', 
        ]);
        $id = $request->input('id');
        $pendaftarMln = PendaftarMlnModel::find($id);

        $pendaftarMln->nama_ayah = $request->input('nama_ayah');
        $pendaftarMln->pendidikan_ayah = $request->input('pendidikan_ayah');
        $pendaftarMln->pekerjaan_ayah = $request->input('pekerjaan_ayah');
        $pendaftarMln->nama_ibu = $request->input('nama_ibu');
        $pendaftarMln->pendidikan_ibu = $request->input('pendidikan_ibu');
        $pendaftarMln->pekerjaan_ibu = $request->input('pekerjaan_ibu');
        $pendaftarMln->nama_wali = $request->input('nama_wali');
        $pendaftarMln->pendidikan_wali = $request->input('pendidikan_wali');
        $pendaftarMln->pekerjaan_wali = $request->input('pekerjaan_wali');
        $pendaftarMln->hubungan_wali_anak = $request->input('hubungan_wali_anak');
        $pendaftarMln->penghasilan_orangtua = $request->input('penghasilan_orangtua');
        $pendaftarMln->penanggung_biaya = $request->input('penanggung_biaya');
        $pendaftarMln->alamat_lengkap = $request->input('alamat_lengkap');
        $pendaftarMln->provinsi = $request->input('provinceId');
        $pendaftarMln->kabupaten = $request->input('cityId');
        $pendaftarMln->kecamatan = $request->input('districtId');
        $pendaftarMln->kelurahan = $request->input('villageId');
        $pendaftarMln->no_hp_1 = $request->input('no_hp_1');
        $pendaftarMln->no_hp_2 = $request->input('no_hp_2');
        $pendaftarMln->save();
        
        $statusRegistrasi = (new StatusRegistrasiModel)->where('user_id', $userId)->first();
        $statusRegistrasi->status_data_ortu = 1;
        $statusRegistrasi->save();

        return redirect('/')->with('status', 'Data Orang Tua berhasil disimpan');

    }
    public function updateJurusan(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'pilihan_jurusan_1' => 'required',
            'pilihan_jurusan_2' => 'required',
            'pilihan_jurusan_3' => 'required',
            'pilihan_pesantren' => 'required',
        ]);
        $id = $request->input('id');
        $pendaftarMln = PendaftarMlnModel::find($id);

        $pendaftarMln->pilihan_jurusan_1 = $request->input('pilihan_jurusan_1');
        $pendaftarMln->pilihan_jurusan_2 = $request->input('pilihan_jurusan_2');
        $pendaftarMln->pilihan_jurusan_3 = $request->input('pilihan_jurusan_3');
        $pendaftarMln->pilihan_pesantren = $request->input('pilihan_pesantren');

        $pendaftarMln->save();
        
        $statusRegistrasi = (new StatusRegistrasiModel)->where('user_id', $userId)->first();
        $statusRegistrasi->status_data_jurusan = 1;
        $statusRegistrasi->save();

        return redirect('/')->with('status', 'Data Jurusan berhasil disimpan');

    }
    public function update(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'nisn' => 'required',
            'tahun_lulus' => 'required',
            'tempat_lahir' => 'required',
            'hari' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
            'jenis_kelamin' => 'required',
            'anak_ke' => 'required',
            'dari_bersaudara' => 'required',
            'sekolah_asal' => 'required',
            'alamat_sekolah_asal' => 'required',
            'kategori_sekolah_asal' => 'required',
            'rencana_tinggal' => 'required',
            'dorongan_masuk' => 'required',
            'sumber_informasi' => 'required'
        ]);
        $id = $request->input('id');
        $pendaftarMln = PendaftarMlnModel::find($id);
        
        $hari = $request->input('hari');
        $bulan = $request->input('bulan');
        $tahun = $request->input('tahun');
        $tanggal_lahir = $tahun.'-'.$bulan.'-'.$hari;

        $pendaftarMln->nama_lengkap = $request->input('nama_lengkap');
        $pendaftarMln->nisn = $request->input('nisn');
        $pendaftarMln->tahun_lulus = $request->input('tahun_lulus');
        $pendaftarMln->tempat_lahir = $request->input('tempat_lahir');
        $pendaftarMln->tanggal_lahir = $tanggal_lahir;
        $pendaftarMln->jenis_kelamin = $request->input('jenis_kelamin');
        $pendaftarMln->anak_ke = $request->input('anak_ke');
        $pendaftarMln->dari_bersaudara = $request->input('dari_bersaudara');
        $pendaftarMln->sekolah_asal = $request->input('sekolah_asal');
        $pendaftarMln->alamat_sekolah_asal = $request->input('alamat_sekolah_asal');
        $pendaftarMln->kategori_sekolah_asal = $request->input('kategori_sekolah_asal');
        $pendaftarMln->rencana_tinggal = $request->input('rencana_tinggal');
        $pendaftarMln->dorongan_masuk = $request->input('dorongan_masuk');
        $pendaftarMln->sumber_informasi = $request->input('sumber_informasi');
        $pendaftarMln->user_id = $userId;
        $pendaftarMln->save();
 

        return redirect('/')->with('status', 'Data Calon Santri berhasil diupdate');
    }

    public function updatePembayaran(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'bank_pengirim' => 'required',
            'nama_pengirim' => 'required',
            'norek_pengirim' => 'required',
            'bank_penerima' => 'required',
            'jumlah_transfer' => 'required',
            'tanggal_transfer' => 'required',
        ]);
        $id = $request->input('id');
        $pendaftarMln = PendaftarMlnModel::find($id);
        $pembayaranMln = new PembayaranMlnModel;
        
        $pembayaranMln->user_id = $pendaftarMln->user_id;
        $pembayaranMln->nama_lengkap = $pendaftarMln->nama_lengkap;
        $pembayaranMln->bank_pengirim = $request->input('bank_pengirim');
        $pembayaranMln->nama_pengirim = $request->input('nama_pengirim');
        $pembayaranMln->norek_pengirim = $request->input('norek_pengirim');
        $pembayaranMln->bank_penerima = $request->input('bank_penerima');
        $pembayaranMln->jumlah_transfer = $request->input('jumlah_transfer');
        $pembayaranMln->tanggal_transfer = $request->input('tanggal_transfer');
        $pembayaranMln->pendaftar_id = $id;
         $pembayaranMln->status = 'Menunggu Validasi';

        $pembayaranMln->save();
        
        $statusRegistrasi = (new StatusRegistrasiModel)->where('user_id', $userId)->first();
        $statusRegistrasi->status_data_pembayaran = 1;
        $statusRegistrasi->save();

        return redirect('/')->with('status', 'Konfirmasi pembayaran diterima. Pembayaran akan divalidasi oleh panitia.');

    }
    //Function
    public function generateNoTes($userId){
         //generate no ujian
        $pendaftarMln = new PendaftarMlnModel;
        $dataCalonSantri = $pendaftarMln->where('user_id', $userId)->first();
        $idCalonSantri = $dataCalonSantri->id;
        $jkCalonSantri = $dataCalonSantri->jenis_kelamin;
        $noRuang = $this->generateRuangan();

        if($dataCalonSantri && $jkCalonSantri == 'laki-laki'){
            $noUjian = 'MA'.$noRuang.'-'.str_pad($idCalonSantri, 3, "0", STR_PAD_LEFT);
        }else{
            $noUjian = 'MB'.$noRuang.'-'.str_pad($idCalonSantri, 3, "0", STR_PAD_LEFT);
        }

        return $noUjian;
    }

    public function generateRuangan(){
        //cek rangan
        $idStatusRuangan = 1;
        $statusRuangan =  StatusRuanganModel::find($idStatusRuangan);
        $jmlPeserta = $statusRuangan->jumlah_peserta;
        $noRuang = $statusRuangan->no_ruang;

        if($jmlPeserta % 20 != 0 && $jmlPeserta > 20){
            $statusRuangan->no_ruang = $noRuang++;
            $statusRuangan->save();
        }

        return str_pad($noRuang, 2, "0", STR_PAD_LEFT);

    }
    public function getProvinces(){
        return Facade::allProvinces();
    }
    public function getCities($provinceId){
        return Facade::findProvince($provinceId, 'cities');
    }
    public function getDistricts($cityId){
        return Facade::findCity($cityId, 'districts');
    }
    public function getVillages($districtId){
        return Facade::findDistrict($districtId, 'villages');
    }
}