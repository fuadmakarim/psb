@extends('Layouts::adminLayout')
@section('content')
<div class="container-fluid">
    <ul class="nav nav-pills nav-fill mt-3" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link {{$type == "list" ? 'active' : ''}}" 
               id="home-tab" 
               data-toggle="tab" 
               href="#home" 
               role="tab" 
               aria-controls="home" 
               aria-selected="{{$type == "list" ? 'true' : 'false'}}">
               Data
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{$type == "form" ? 'active' : ''}}" 
               id="profile-tab" 
               data-toggle="tab" 
               href="#profile" 
               role="tab" 
               aria-controls="profile" 
               aria-selected="{{$type == "form" ? 'true' : 'false'}}">
               Input Data
            </a>
        </li>
    </ul>
   
</div>
 <hr>
<div class="container-fluid">
    
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade {{$type == "list" ? 'show active' : ''}}" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div class="row">
                <div class="col-md-12">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="card bg-white mb-3">
                        <div class="card-header"><h5>Data Pendaftar {{$title}}</h5>
                            {{-- <div class="col-md-4 float-right">
                                <a href="" class="btn btn-primary btn-block">+ Input Data</a>
                            </div> --}}
                            {{-- <div class="col-md-4 float-right">
                                <div class="form-group row">
                                <select class="form-control col-md-12" name="" id="">
                                    <option>Semua</option>
                                    <option></option>
                                    <option></option>
                                </select>
                                </div>
                            </div>
                             --}}
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No Tes</th>
                                        <th>Nama Pendaftar</th>
                                        <th>Asal Sekolah</th>
                                        <th>Waktu</th>
                                        <th>Petugas Pendaftaran</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataPendaftar as $data)
                                    <tr>
                                    <td scope="row">{{$data->id}}</td>
                                    <td>{{$data->no_tes}}</td>
                                        <td>{{$data->nama_lengkap}}</td>
                                        <td>{{$data->sekolah_asal}}</td>
                                        <td>{{$data->created_at}}</td>
                                        <td>{{$data->petugas_pendaftaran}}</td>
                                        <td>{{$data->no_tes != null ? 'AKTIF' : 'BELUM AKTIF'}}</td>
                                        <td> <a href="{{url('cetak-kwitansi/'.$data->id)}}" class="btn btn-success btn-block">Cetak Kwitansi</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade {{$type == "form" ? 'show active' : ''}}" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="col-md-12">
           
                    {{-- <div class="form-group">
                        <select class="col-md-12 selectpicker" id="dataSantri" data-live-search="true">
                        </select>
                    </div> --}}
                    <div class="mb-3">
                        <input type="text" name="data_santri" class="form-control border-success" placeholder="Cari data santri" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div id="optionSantri" class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                        </div>
                    </div>
  
            </div>
            <form action="{{url('/admin/pendaftaran/create')}}"  
              method="POST" 
              role="form" 
              enctype="multipart/form-data" 
              class="col-md-12">
                {{ csrf_field() }}
                @if($dataCalonSantri)
                <input type="hidden" name="id" value="{{$dataCalonSantri->id}}"/>
                @endif
                <div class="row">
                    <div class="col-md-6"> 
                        <div class="card bg-light mb-3">
                            <div class="card-header"><span>Data Calon Santri</span></div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Jalur Masuk</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="jalur_masuk" required>
                                            <option value="Reguler">Reguler</option>
                                            <option value="Beasiswa">Beasiswa</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Nama Lengkap</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            name="nama_lengkap" 
                                            class="form-control form-control-sm" 
                                            placeholder="Nama lengkap" 
                                            required
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nama_lengkap}}"
                                            />
                                        {{-- <span>Wajib menggunakan hurup kapital, contoh: MUHAMMAD AHSAN</span> --}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">NISN</label>
                                    <div class="col-md-9">
                                                <input 
                                                    type="text" 
                                                    name="nisn" 
                                                    class="form-control form-control-sm" 
                                                    placeholder="NISN" 
                                                    required
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nisn}}">
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Hobi</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            name="hobi" 
                                            class="form-control form-control-sm" 
                                            placeholder="Hobi" 
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->hobi}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Cita-cita</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            name="cita_cita" 
                                            class="form-control form-control-sm" 
                                            placeholder="Cita-cita" 
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->cita_cita}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Tempat & Tanggal Lahir</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input 
                                                    type="text" 
                                                    class="form-control form-control-sm" 
                                                    name="tempat_lahir" 
                                                    placeholder="Tempat lahir" 
                                                    required
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->tempat_lahir}}">
                                            </div>
                                            @if($dataCalonSantri)
                                            @php
                                                $tanggal = explode('/', $dataCalonSantri->tanggal_lahir);
                                                $hari = $tanggal[1];
                                                $bulan = $tanggal[0];
                                                $tahun = $tanggal[2];
                                            @endphp
                                            @else
                                            @php
                                                $hari = null;
                                                $bulan = null;
                                                $tahun = null;
                                            @endphp
                                            @endif
                                            <div class="col-md-3">
                                                <select class="form-control form-control-sm" name="hari" required>
                                                    <option value="{{!$hari ? '' : $hari}}">
                                                        {{!$hari ? 'Tanggal' : $hari}}
                                                    </option>
                                                    @for($i=1; $i<=31; $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control form-control-sm" name="bulan" required>
                                                    <option value="{{!$bulan ? '' : $bulan}}">
                                                        {{!$bulan ? 'Bulan' : $bulan}}
                                                    </option>
                                                    @for($i=1; $i<=12; $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control form-control-sm" name="tahun" required>
                                                    <option value="{{!$tahun ? '' : $tahun}}">
                                                        {{!$tahun ? 'Tahun' : $tahun}}
                                                    </option>
                                                    @for($i=2001; $i<=2010; $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Jenis Kelamin</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="jenis_kelamin" required>
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->jenis_kelamin}}">
                                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->jenis_kelamin}}
                                            </option>
                                            <option value="laki-laki">Laki-laki</option>
                                            <option value="perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Anak ke</label>
                                    <div class="col-md-2">
                                        <input 
                                            type="number" 
                                            class="form-control form-control-sm" 
                                            name="anak_ke" 
                                            placeholder="" 
                                            required
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->anak_ke}}">
                                    </div>
                                    <label class="col-md-2 col-form-label col-form-label-sm">dari</label>
                                    <div class="col-md-2">
                                        <input 
                                            type="number" 
                                            class="form-control form-control-sm" 
                                            name="dari_bersaudara"
                                            placeholder="" 
                                            required
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->dari_bersaudara}}">
                                    </div>
                                    <label class="col-md-2 col-form-label col-form-label-sm">bersaudara</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Nama Sekolah Asal</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input 
                                                    type="text" 
                                                    class="form-control form-control-sm" 
                                                    name="sekolah_asal"
                                                    placeholder="Nama Sekolah Asal" 
                                                    required
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->sekolah_asal}}">
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control form-control-sm" name="status_sekolah">
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->status_sekolah}}">
                                                            {{!$dataCalonSantri ? 'Status Sekolah' : $dataCalonSantri->status_sekolah}}
                                                    </option>
                                                    <option value="Negeri">Negeri</option>
                                                    <option value="Swasta">Swasta</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Alamat Sekolah Asal</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input 
                                                    type="text" 
                                                    class="form-control form-control-sm" 
                                                    name="alamat_sekolah_asal"
                                                    placeholder="Alamat Sekolah Asal" 
                                                    required
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->alamat_sekolah_asal}}">
                                            </div>
                                            <div class="col-md-4">
                                                <select name="tahun_lulus" class="form-control form-control-sm">
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->tahun_lulus}}">
                                                        {{!$dataCalonSantri ? 'Tahun Lulus' : $dataCalonSantri->tahun_lulus}}
                                                    </option>
                                                    <option value="2018">2018</option>
                                                    <option value="2019">2019</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Kategori Sekolah Asal</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="kategori_sekolah_asal" required>
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->kategori_sekolah_asal}}">
                                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->kategori_sekolah_asal}}
                                            </option>
                                            <option value="MTS Persis Tarogong">MTs Persis Tarogong</option>
                                            <option value="MTs Persis Lainn">MTs Persis Lain</option>
                                            <option value="SMP Islam">SMP Islam</option>
                                            <option value="SMP Umum/Negeri">SMP Umum/Negeri</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Rencana tinggal saat belajar</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="rencana_tinggal" required>
                                            <option {{!$dataCalonSantri ? '' : 'value='.$dataCalonSantri->rencana_tinggal}}>
                                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->rencana_tinggal}}
                                            </option>
                                            <option value="Asrama">Asrama</option>
                                            <option value="Rumah Orang Tua">Rumah Orang Tua</option>
                                            <option value="Rumah Saudara / Wali">Rumah Saudara / Wali</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Dorongan masuk pesantren</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="dorongan_masuk" >
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->dorongan_masuk}}">
                                                           {{!$dataCalonSantri ? '' : $dataCalonSantri->dorongan_masuk}}
                                            </option>
                                            <option value="Diri Sendiri">Diri Sendiri</option>
                                            <option value="Orang Tua">Orang Tua</option>
                                            <option value="Saudara">Saudara</option>
                                            <option value="Teman">Teman</option>
                                            <option value="Lainnya">Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Sumber informasi PSB</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="sumber_informasi" id="" >
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->sumber_informasi}}">
                                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->sumber_informasi}}
                                            </option>
                                            <option value="Keluarga">Keluarga</option>
                                            <option value="Teman">Teman</option>
                                            <option value="Brosur">Brosur</option>
                                            <option value="Majalah">Majalah</option>
                                            <option value="Internet">Website</option>
                                            <option value="Sekolah">Sekolah</option>
                                            <option value="Medsos">Medsos</option>
                                            <option value="Spanduk">Spanduk</option>
                                            <option value="Lainnya">Lainnya</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-light mb-3">
                            <div class="card-header"><span>Pilihan Jurusan</span></div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Pilihan 1</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select name="pilihan_jurusan_1" class="form-control form-control-sm" required>
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_1}}">
                                                        {{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_1}}
                                                </option>
                                                    <option value='IAI'>IAI</option>
                                                    <option value='IPA'>IPA</option>
                                                    <option value='IPS'>IPS</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Pilihan 2</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select name="pilihan_jurusan_2" class="form-control form-control-sm" required>
                                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_2}}">
                                                        {{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_2}}
                                                </option>
                                                    <option value='IAI'>IAI</option>
                                                    <option value='IPA'>IPA</option>
                                                    <option value='IPS'>IPS</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Pilihan 3</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select name="pilihan_jurusan_3" class="form-control form-control-sm" >
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_3}}">
                                                        {{!$dataCalonSantri ? '' : $dataCalonSantri->pilihan_jurusan_3}}
                                                </option>
                                                    <option value='IAI'>IAI</option>
                                                    <option value='IPA'>IPA</option>
                                                    <option value='IPS'>IPS</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-6 col-form-label col-form-label-sm">Bersedia ditempatkan di jurusan manapun berdasarkan pertimbangan pesantren? </label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="pilihan_pesantren" id="" value="1" checked>
                                                    Ya
                                                </label>
                                                </div>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="pilihan_pesantren" id="" value="0">
                                                    Tidak
                                                </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card bg-light mb-3">
                            <div class="card-header"><span>Data Orang Tua</span></div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Data Ayah Kandung</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            name="nama_ayah" 
                                            class="form-control form-control-sm" 
                                            placeholder="Nama Lengkap" 
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nama_ayah}}"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm"></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select name="pendidikan_ayah" class="form-control form-control-sm" >
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pendidikan_ayah}}">
                                                        {{!$dataCalonSantri ? 'Pendidikan' : $dataCalonSantri->pendidikan_ayah}}
                                                    </option>
                                                    <option value="SD">SD</option>
                                                    <option value="SMP">SMP</option>
                                                    <option value="SMA">SMA</option>
                                                    <option value="D1">D1</option>
                                                    <option value="D2">D2</option>
                                                    <option value="D3">D3</option>
                                                    <option value="D4">D4</option>
                                                    <option value="S1">S1</option>
                                                    <option value="S2">S2</option>
                                                    <option value="S3">S3</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="pekerjaan_ayah" class="form-control form-control-sm" >
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pekerjaan_ayah}}">
                                                        {{!$dataCalonSantri ? 'Pekerjaan' : $dataCalonSantri->pekerjaan_ayah}}
                                                    </option>
                                                    <option value="Tidak bekerja (Di rumah saja)">1 - Tidak bekerja (Di rumah saja)</option>
                                                    <option value="Pensiunan/Almarhum">2 - Pensiunan/Almarhum</option>
                                                    <option value="PNS">3 - PNS</option>
                                                    <option value="TNI/Polisi">4 - TNI/Polisi</option>
                                                    <option value="Guru/Dosen">5 - Guru/Dosen</option>
                                                    <option value="Pegawai Swasta">6 - Pegawai Swasta</option>
                                                    <option value="Pengusaha/Wiraswasta">7 - Pengusaha/Wiraswasta</option>
                                                    <option value="Pengacara/Hakim/Jaksa/Notaris">8 - Pengacara/Hakim/Jaksa/Notaris</option>
                                                    <option value="Seniman/Pelukis/Artis">9 - Seniman/Pelukis/Artis</option>
                                                    <option value="Dokter/Bidan/Perawat">10 - Dokter/Bidan/Perawat</option>
                                                    <option value="Pilot/Pramugari">11 - Pilot/Pramugari</option>
                                                    <option value="Pedagang">12 - Pedagang</option>
                                                    <option value="Petani/Peternak">13 - Petani/Peternak</option>
                                                    <option value="Nelayan">14 - Nelayan</option>
                                                    <option value="Buruh (Tani/Pabrik/Bangunan)">15 - Buruh (Tani/Pabrik/Bangunan)</option>
                                                    <option value="Sopir/Masinis/Kondektur">16 - Sopir/Masinis/Kondektur</option>
                                                    <option value="Politikus">17 - Politikus</option>
                                                    <option value="Lainnya">18 - Lainnya</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Data Ibu Kandung</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            name="nama_ibu" 
                                            class="form-control form-control-sm" 
                                            placeholder="Nama lengkap" 
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nama_ibu}}"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm"></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select name="pendidikan_ibu" class="form-control form-control-sm" >
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pendidikan_ibu}}">
                                                        {{!$dataCalonSantri ? 'Pendidikan' : $dataCalonSantri->pendidikan_ibu}}
                                                    </option>
                                                    <option value="SD">SD</option>
                                                    <option value="SMP">SMP</option>
                                                    <option value="SMA">SMA</option>
                                                    <option value="D1">D1</option>
                                                    <option value="D2">D2</option>
                                                    <option value="D3">D3</option>
                                                    <option value="D4">D4</option>
                                                    <option value="S1">S1</option>
                                                    <option value="S2">S2</option>
                                                    <option value="S3">S3</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="pekerjaan_ibu" class="form-control form-control-sm" >
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->pekerjaan_ibu}}">
                                                        {{!$dataCalonSantri ? 'Pekerjaan' : $dataCalonSantri->pekerjaan_ibu}}
                                                    </option>
                                                    <option value="Ibu Rumah Tangga">1 - Ibu Rumah Tangga</option>
                                                    <option value="Pensiunan/Almarhum">2 - Pensiunan/Almarhum</option>
                                                    <option value="PNS">3 - PNS</option>
                                                    <option value="TNI/Polisi">4 - TNI/Polisi</option>
                                                    <option value="Guru/Dosen">5 - Guru/Dosen</option>
                                                    <option value="Pegawai Swasta">6 - Pegawai Swasta</option>
                                                    <option value="Pengusaha/Wiraswasta">7 - Pengusaha/Wiraswasta</option>
                                                    <option value="Pengacara/Hakim/Jaksa/Notaris">8 - Pengacara/Hakim/Jaksa/Notaris</option>
                                                    <option value="Seniman/Pelukis/Artis">9 - Seniman/Pelukis/Artis</option>
                                                    <option value="Dokter/Bidan/Perawat">10 - Dokter/Bidan/Perawat</option>
                                                    <option value="Pilot/Pramugari">11 - Pilot/Pramugari</option>
                                                    <option value="Pedagang">12 - Pedagang</option>
                                                    <option value="Petani/Peternak">13 - Petani/Peternak</option>
                                                    <option value="Nelayan">14 - Nelayan</option>
                                                    <option value="Buruh (Tani/Pabrik/Bangunan)">15 - Buruh (Tani/Pabrik/Bangunan)</option>
                                                    <option value="Sopir/Masinis/Kondektur">16 - Sopir/Masinis/Kondektur</option>
                                                    <option value="Politikus">17 - Politikus</option>
                                                    <option value="Lainnya">18 - Lainnya</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Penghasilan Orang Tua / Wali</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="penghasilan_orangtua" required>
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->penghasilan_orangtua}}">
                                                {{!$dataCalonSantri ? '' : $dataCalonSantri->penghasilan_orangtua}}
                                            </option>
                                            <option value='< Rp 500.000'>< Rp 500.000</option>
                                            <option value='Rp 500.001 - Rp 1.000.000'>Rp 500.001 - Rp 1.000.000</option>
                                            <option value='Rp 1.000.001 - Rp 2.000.000'>Rp 1.000.001 - Rp 2.000.000</option>
                                            <option value='Rp 2.000.001 - Rp 3.000.000'>Rp 2.000.001 - Rp 3.000.000</option>
                                            <option value='Rp 3.000.001 - Rp 5.000.000'>Rp 3.000.001 - Rp 5.000.000</option>
                                            <option value='> Rp 5.000.000'>> Rp 5.000.000</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Penanggung Biaya Pendidikan</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="penanggung_biaya" required>
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->penanggung_biaya}}">
                                                {{!$dataCalonSantri ? '' : $dataCalonSantri->penanggung_biaya}}
                                            </option>
                                            <option value='Orang Tua'>Orang Tua</option>
                                            <option value='Saudara'>Saudara</option>
                                            <option value='Wali'>Wali</option>
                                            <option value='Beasiswa'>Beasiswa</option>
                                            <option value='Lainnya'>Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Alamat Lengkap</label>
                                    <div class="col-md-9">
                                        <textarea 
                                            type="text" 
                                            name="alamat_lengkap" 
                                            class="form-control form-control-sm"
                                            required>{{!$dataCalonSantri ? '' : $dataCalonSantri->alamat_lengkap}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Provinsi</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select name="provinceId" class="form-control form-control-sm" required>
                                                <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->provinsi}}">
                                                        {{!$dataCalonSantri ? 'Silahkan pilih provinsi' : $dataCalonSantri->provinsi}}
                                                </option>
                                                @foreach($provinces as $province)
                                                    <option data-id="{{$province->id}}" value='{{$province->name}}'>{{$province->name}}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Kabupaten / Kota</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select name='cityId' class="form-control form-control-sm" required>
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->kabupaten}}">
                                                        {{!$dataCalonSantri ? '' : $dataCalonSantri->kabupaten}}
                                                    </option>
                                                </select>
                                                @if ($errors->all())
                                                    <span class="help-block">
                                                        <strong style="color:red">{{ $errors->first('cityId') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Kecamatan</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select name='districtId' class="form-control form-control-sm" required>
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->kecamatan}}">
                                                        {{!$dataCalonSantri ? '' : $dataCalonSantri->kecamatan}}
                                                    </option>
                                                </select>
                                                @if ($errors->all())
                                                    <span class="help-block">
                                                        <strong style="color:red">{{ $errors->first('districtId') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Kelurahan / Desa</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select name="villageId" class="form-control form-control-sm" required>
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->kelurahan}}">
                                                        {{!$dataCalonSantri ? '' : $dataCalonSantri->kelurahan}}
                                                    </option>
                                                </select>
                                                @if ($errors->all())
                                                    <span class="help-block">
                                                        <strong style="color:red">{{ $errors->first('villageId') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>           
                                <hr>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">No HP / WA</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input 
                                                    type="number" 
                                                    name="no_hp_1" 
                                                    class="form-control form-control-sm" 
                                                    placeholder="No HP / WA 1" 
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->no_hp_1}}"
                                                    required>
                                            </div>
                                            <div class="col-md-6">
                                                <input 
                                                    type="number" 
                                                    name="no_hp_2" 
                                                    class="form-control form-control-sm" 
                                                    placeholder="No HP / WA 2" 
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->no_hp_2}}"
                                                    >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card bg-light mb-3">
                            <div class="card-header"><span>Data Wali (jika ada)</span></div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Nama Wali</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input 
                                                    type="text" 
                                                    name="nama_wali" 
                                                    class="form-control form-control-sm" 
                                                    placeholder="Nama Lengkap" 
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nama_wali}}">
                                            </div>
                                            <div class="col-md-6">
                                                <select name="pekerjaan_wali" class="form-control form-control-sm">
                                                    <option {{!$dataCalonSantri ? '' : 'value='.$dataCalonSantri->pekerjaan_wali}}>
                                                        {{!$dataCalonSantri ? 'Pekerjaan' : $dataCalonSantri->pekerjaan_wali}}
                                                    </option>
                                                    <option value="Tidak bekerja (Di rumah saja)">1 - Tidak bekerja (Di rumah saja)</option>
                                                    <option value="Pensiunan/Almarhum">2 - Pensiunan/Almarhum</option>
                                                    <option value="PNS">3 - PNS</option>
                                                    <option value="TNI/Polisi">4 - TNI/Polisi</option>
                                                    <option value="Guru/Dosen">5 - Guru/Dosen</option>
                                                    <option value="Pegawai Swasta">6 - Pegawai Swasta</option>
                                                    <option value="Pengusaha/Wiraswasta">7 - Pengusaha/Wiraswasta</option>
                                                    <option value="Pengacara/Hakim/Jaksa/Notaris">8 - Pengacara/Hakim/Jaksa/Notaris</option>
                                                    <option value="Seniman/Pelukis/Artis">9 - Seniman/Pelukis/Artis</option>
                                                    <option value="Dokter/Bidan/Perawat">10 - Dokter/Bidan/Perawat</option>
                                                    <option value="Pilot/Pramugari">11 - Pilot/Pramugari</option>
                                                    <option value="Pedagang">12 - Pedagang</option>
                                                    <option value="Petani/Peternak">13 - Petani/Peternak</option>
                                                    <option value="Nelayan">14 - Nelayan</option>
                                                    <option value="Buruh (Tani/Pabrik/Bangunan)">15 - Buruh (Tani/Pabrik/Bangunan)</option>
                                                    <option value="Sopir/Masinis/Kondektur">16 - Sopir/Masinis/Kondektur</option>
                                                    <option value="Politikus">17 - Politikus</option>
                                                    <option value="Lainnya">18 - Lainnya</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Hubungan dengan Anak</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input 
                                                    type="text" 
                                                    name="hubungan_wali_anak" 
                                                    class="form-control form-control-sm" 
                                                    placeholder="" 
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->hubungan_wali_anak}}">
                                            </div>
                                            <div class="col-md-6">
                                                <input 
                                                    type="number" 
                                                    name="no_hp_1" 
                                                    class="form-control form-control-sm" 
                                                    placeholder="No HP / WA Wali" 
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->no_hp_wali}}"
                                                    >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Alamat Wali</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            name="hobi" 
                                            class="form-control form-control-sm" 
                                            placeholder="Alamat Wali" 
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->alamat_wali}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-light mb-3">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Petugas Pendaftaran</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            name="petugas_pendaftaran" 
                                            class="form-control form-control-sm" 
                                            placeholder="Nama" 
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->petugas_pendaftaran}}"
                                            required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <button type="submit" class="btn btn-primary float-right btn-block">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<br><br><br>

@endsection