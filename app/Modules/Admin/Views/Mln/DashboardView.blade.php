@extends('Layouts::adminLayout')
@section('content')
    <div class="container-fluid">
        
        <div class="card-columns mt-3">
           
                <div class="card text-white bg-info mb-3">
                    <div class="card-header">Pendaftar berdasarkan Jenis Kelamin</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarJk))
                                    @foreach ($dataPendaftarJk as $data)
                                        <tr>
                                            <td>{{$data->jenis_kelamin}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 2)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            

           
                <div class="card text-white bg-info mb-3">
                    <div class="card-header">Pendaftar berdasarkan Jalur Masuk</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarJalurMasuk))
                                    @foreach ($dataPendaftarJalurMasuk as $data)
                                        <tr>
                                            <td>{{$data->jalur_masuk}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 2)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card text-white bg-info mb-3">
                    <div class="card-header">Pendaftar berdasarkan Sekolah Asal</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Kategori Sekolah Asal</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarSekolahAsal))
                                    @foreach ($dataPendaftarSekolahAsal as $data)
                                        <tr>
                                            <td>{{$data->kategori_sekolah_asal}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 2)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card text-white bg-info mb-3">
                    <div class="card-header">Pendaftar berdasarkan Sekolah Asal</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Kategori Sekolah Asal</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarRencanaTinggal))
                                    @foreach ($dataPendaftarRencanaTinggal as $data)
                                        <tr>
                                            <td>{{$data->rencana_tinggal}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 2)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                
           
        </div>
    </div>
@endsection