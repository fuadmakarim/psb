@extends('Layouts::adminLayout')
@section('content')
<div class="container-fluid">
    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
    <div class="card bg-white mb-3 mt-3">
        <div class="card-header"><h5>Data Pembayaran</h5></div>
    
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Pendaftar</th>
                        <th>Ke Rekening</th>
                        <th>Dari Rekening</th>
                        <th>Jumlah Transfer</th>
                        <th>Tanggal Transfer</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataPembayaran as $data)
                    <tr>
                        <td scope="row">{{$data->id}}</td>
                        <td>{{$data->nama_lengkap}}</td>
                        <td>{{$data->sekolah_asal}}</td>
                        <td>{{$data->bank_pengirim}}</td>
                        <td>{{$data->bank_penerima}}</td>
                        <td>{{$data->tanggal_transfer}}</td>
                        <td>
                            @if($data->status == 'Dibayar')
                                Dibayar
                            @else
                                <a href="{{url('admin/pembayaran/validasi/'.$data->id.'/'.$data->pendaftar_id)}}" class="btn btn-primary">Validasi Pembayaran</a></td>
                            @endif
                        <td> <a href="{{url('cetak-kartu/'.$data->id)}}" class="btn btn-success btn-block">Cetak Kartu Peserta</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection