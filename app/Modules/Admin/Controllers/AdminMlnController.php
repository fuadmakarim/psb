<?php

namespace App\Modules\Admin\Controllers;
use Validator;
use Illuminate\Http\Request;
use Laravolt\Indonesia\Facade;
use App\Http\Controllers\Controller;
use App\Modules\Registrasi\Models\PendaftarMlnModel;
use App\Modules\Registrasi\Models\PembayaranMlnModel;
use App\Modules\Registrasi\Models\StatusRegistrasiModel;
use App\Modules\Registrasi\Models\StatusRuanganModel;
use App\Modules\Admin\Models\DataSantriTsnModel;
use Illuminate\Support\Facades\Auth;

class AdminMlnController extends Controller
{
    //Data
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $user = Auth::user()->getAttributes();
            $role = $user['role'];
            if($role == 'admin_mln'){
                return $next($request);
            }else{
                return redirect('/login');
            }
        });
    }

    public function index(){
        $dataPendaftar = PendaftarMlnModel::orderBy('id', 'desc')->get();
        $dataCalonSantri = null;
        $provinces = $this->getProvinces();
        $type = "list";
        $title = "";
    	return view('Admin::Mln.ListSantriView', array(
            'dataPendaftar' => $dataPendaftar,
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
            'title' => $title
        ));
    }

    public function dashboard(){
        $pendaftarMln = new PendaftarMlnModel;
        $dataPendaftar = $pendaftarMln->get();
        $total = count($dataPendaftar);
        $dataPendaftarJk = $pendaftarMln->getCountDataSantri('jenis_kelamin');
        $dataPendaftarJalurMasuk = $pendaftarMln->getCountDataSantri('jalur_masuk');
        $dataPendaftarSekolahAsal = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        $dataPendaftarRencanaTinggal = $pendaftarMln->getCountDataSantri('rencana_tinggal');
        // $dataPendaftarCaraPendaftaran = $pendaftarMln->getCountDataSantri('is_admin');
        // $dataPendaftarPilihanJurusan = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarPenghasilanOrtu = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarPenanggungBiaya = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarMotifMasuk = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarPekerjaanOrtu = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarKotaAsal = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');

        return view('Admin::Mln.DashboardView', array(
            'dataPendaftarJk' => $dataPendaftarJk,
            'dataPendaftarJalurMasuk' =>  $dataPendaftarJalurMasuk,
            'dataPendaftarSekolahAsal' => $dataPendaftarSekolahAsal,
            'dataPendaftarRencanaTinggal' => $dataPendaftarRencanaTinggal,
            'total' => $total,
        ));
    }

    public function indexPendaftarOnline(){
        $dataPendaftar = (new PendaftarMlnModel)->getPendaftarOnline();
        // dd($dataPendaftar);
        $dataCalonSantri = null;
        $provinces = $this->getProvinces();
        $type = "list";
        $title = "Online";
    	return view('Admin::Mln.ListSantriView', array(
            'dataPendaftar' => $dataPendaftar,
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
            'title' => $title
        ));
    }

    public function indexPendaftarFinal(){
        $dataPendaftar = (new PendaftarMlnModel)->getPendaftarFinal();
        $dataCalonSantri = null;
        $provinces = $this->getProvinces();
        $type = "list";
        $title = "Final";
    	return view('Admin::Mln.ListSantriView', array(
            'dataPendaftar' => $dataPendaftar,
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
            'title' => $title
        ));
    }

    public function add($id){
        $dataPendaftar = PendaftarMlnModel::orderBy('id', 'desc')->get();
        $dataCalonSantri = DataSantriTsnModel::find($id);
        $provinces = $this->getProvinces();
        $type = "form";
        return view('Admin::Mln.ListSantriView', array(
            'dataPendaftar' => $dataPendaftar,
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
        ));
    }

    public function create(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'nisn' => 'required',
            'tahun_lulus' => 'required',
            'tempat_lahir' => 'required',
            'hari' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
            'jenis_kelamin' => 'required',
            'hobi' => 'hobi',
            'cita_cita' => 'cita_cita',
            'anak_ke' => 'required',
            'dari_bersaudara' => 'required',
            'sekolah_asal' => 'required',
            'alamat_sekolah_asal' => 'required',
            'kategori_sekolah_asal' => 'required',
            'rencana_tinggal' => 'required',
            'dorongan_masuk' => 'required',
            'sumber_informasi' => 'required',
            'nama_ayah' => 'required',
            'pendidikan_ayah' => 'required',
            'pekerjaan_ayah' => 'required',
            'nama_ibu' => 'required',
            'pendidikan_ibu' => 'required',
            'pekerjaan_ibu' => 'required',
            'penghasilan_orangtua' => 'required',
            'penanggung_biaya' => 'required',
            'alamat_lengkap' => 'required',
            'provinceId' => 'required',
            'cityId' => 'required',
            'districtId' => 'required',
            'villageId' => 'required',
            'no_hp_1' => 'required',
            'no_hp_2' => 'required', 
            'pilihan_jurusan_1' => 'required',
            'pilihan_jurusan_2' => 'required',
            'pilihan_jurusan_3' => 'required',
            'pilihan_pesantren' => 'required',
            'jalur_masuk' => 'required',
        ]);
        $pendaftarMln = new PendaftarMlnModel;
        
        $hari = $request->input('hari');
        $bulan = $request->input('bulan');
        $tahun = $request->input('tahun');
        $tanggal_lahir = $tahun.'-'.$bulan.'-'.$hari;
        
        $pendaftarMln->is_admin = 1;
        $pendaftarMln->nama_lengkap = $request->input('nama_lengkap');
        $pendaftarMln->nisn = $request->input('nisn');
        $pendaftarMln->tahun_lulus = $request->input('tahun_lulus');
        $pendaftarMln->tempat_lahir = $request->input('tempat_lahir');
        $pendaftarMln->tanggal_lahir = $tanggal_lahir;
        $pendaftarMln->jenis_kelamin = $request->input('jenis_kelamin');
        $pendaftarMln->hobi = $request->input('hobi');
        $pendaftarMln->cita_cita = $request->input('cita_cita');
        $pendaftarMln->anak_ke = $request->input('anak_ke');
        $pendaftarMln->dari_bersaudara = $request->input('dari_bersaudara');
        $pendaftarMln->sekolah_asal = $request->input('sekolah_asal');
        $pendaftarMln->status_sekolah = $request->input('status_sekolah');
        $pendaftarMln->alamat_sekolah_asal = $request->input('alamat_sekolah_asal');
        $pendaftarMln->kategori_sekolah_asal = $request->input('kategori_sekolah_asal');
        $pendaftarMln->rencana_tinggal = $request->input('rencana_tinggal');
        $pendaftarMln->dorongan_masuk = $request->input('dorongan_masuk');
        $pendaftarMln->sumber_informasi = $request->input('sumber_informasi');
        $pendaftarMln->user_id = $userId;
        $pendaftarMln->nama_ayah = $request->input('nama_ayah');
        $pendaftarMln->pendidikan_ayah = $request->input('pendidikan_ayah');
        $pendaftarMln->pekerjaan_ayah = $request->input('pekerjaan_ayah');
        $pendaftarMln->nama_ibu = $request->input('nama_ibu');
        $pendaftarMln->pendidikan_ibu = $request->input('pendidikan_ibu');
        $pendaftarMln->pekerjaan_ibu = $request->input('pekerjaan_ibu');
        $pendaftarMln->nama_wali = $request->input('nama_wali');
        $pendaftarMln->pendidikan_wali = $request->input('pendidikan_wali');
        $pendaftarMln->pekerjaan_wali = $request->input('pekerjaan_wali');
        $pendaftarMln->hubungan_wali_anak = $request->input('hubungan_wali_anak');
        $pendaftarMln->penghasilan_orangtua = $request->input('penghasilan_orangtua');
        $pendaftarMln->penanggung_biaya = $request->input('penanggung_biaya');
        $pendaftarMln->alamat_lengkap = $request->input('alamat_lengkap');
        $pendaftarMln->provinsi = $request->input('provinceId');
        $pendaftarMln->kabupaten = $request->input('cityId');
        $pendaftarMln->kecamatan = $request->input('districtId');
        $pendaftarMln->kelurahan = $request->input('villageId');
        $pendaftarMln->no_hp_1 = $request->input('no_hp_1');
        $pendaftarMln->no_hp_2 = $request->input('no_hp_2');
        $pendaftarMln->pilihan_jurusan_1 = $request->input('pilihan_jurusan_1');
        $pendaftarMln->pilihan_jurusan_2 = $request->input('pilihan_jurusan_2');
        $pendaftarMln->pilihan_jurusan_3 = $request->input('pilihan_jurusan_3');
        $pendaftarMln->pilihan_pesantren = $request->input('pilihan_pesantren');
        $pendaftarMln->jalur_masuk = $request->input('jalur_masuk');
        $pendaftarMln->petugas_pendaftaran = $request->input('petugas_pendaftaran');
        $pendaftarMln->save();
        
        $id = $pendaftarMln->id;
        $this->updatePembayaran($id);
        return redirect('/admin')->with('status', 'Data berhasil disimpan');
    }

    public function indexPembayaran(){
        $dataPembayaran = PembayaranMlnModel::orderBy('id', 'desc')->get();
    	return view('Admin::Mln.ListSantriPembayaranView', array(
            'dataPembayaran' => $dataPembayaran
        ));
    }
    public function updatePembayaran($id){
        $userId = Auth::id();
        $pendaftarMln = PendaftarMlnModel::find($id);
        $pembayaranMln = new PembayaranMlnModel;
        
        $pembayaranMln->user_id = $pendaftarMln->user_id;
        $pembayaranMln->nama_lengkap = $pendaftarMln->nama_lengkap;
        $pembayaranMln->jumlah_transfer = "Rp300.000";
        $pembayaranMln->pendaftar_id = $id;
        $pembayaranMln->status = 'Menunggu Validasi';

        $pembayaranMln->save();
    }
    public function validasiPembayaran($id, $santriId){
        $userId = Auth::id();
        $dataPendaftar = PendaftarMlnModel::find($santriId);
        $pembayaranMln = PembayaranMlnModel::find($id);
        $date = date("d-m-Y");

        $pembayaranMln->validasi_oleh = $userId;
        $pembayaranMln->status = "Dibayar";
        $pembayaranMln->tanggal_validasi = $date;

        $pembayaranMln->save();

        if($pembayaranMln->status == "Dibayar"){
            $noTes = $this->generateNoTes($santriId, $id);
            $dataPendaftar->no_tes = $noTes;
            $dataPendaftar->save();

            $noRuang = $this->generateRuangan();
            $dataPendaftar->no_ruang = $noRuang;
            $dataPendaftar->save();
        }
        return redirect('/admin/pembayaran')->with('status', 'Pembayaran divalidasi');

    }

    //Function
    public function generateNoTes($santriId, $id){
         //generate no ujian
        $pendaftarMln = new PendaftarMlnModel;
        $dataCalonSantri = $pendaftarMln->where('id', $santriId)->first();
        $idCalonSantri = $id;
        $jkCalonSantri = $dataCalonSantri->jenis_kelamin;
        $noRuang = $this->generateRuangan();

        if($dataCalonSantri && ($jkCalonSantri == 'laki-laki' || $jkCalonSantri == 'L')){
            $noUjian = 'MA'.$noRuang.'-'.str_pad($idCalonSantri, 3, "0", STR_PAD_LEFT);
        }else{
            $noUjian = 'MB'.$noRuang.'-'.str_pad($idCalonSantri, 3, "0", STR_PAD_LEFT);
        }
        return $noUjian;
        
    }

    public function generateRuangan(){
        //cek rangan
        $idStatusRuangan = 1;
        $statusRuangan =  StatusRuanganModel::find($idStatusRuangan);
        $jmlPeserta = $statusRuangan->jumlah_peserta;
        $noRuang = $statusRuangan->no_ruang;

        if($jmlPeserta % 20 != 0 && $jmlPeserta > 20){
            $statusRuangan->no_ruang = $noRuang++;
            $statusRuangan->save();
        }

        return str_pad($noRuang, 2, "0", STR_PAD_LEFT);

    }

    public function getProvinces(){
        return Facade::allProvinces();
    }
    public function getCities($provinceId){
        $data = Facade::findProvince($provinceId, 'cities');
        $cities = $data->cities;
        return $cities;
    }
    public function getDistricts($cityId){
        $data = Facade::findCity($cityId, 'districts');
        $districts = $data->districts;
        return $districts;
    }
    public function getVillages($districtId){
        $data = Facade::findDistrict($districtId, 'villages'); 
        $villages = $data->villages;
        return $villages;
    }

    public function getSantriTsn(Request $request){
        $keyword = $request->input("val");
        $dataSantri = new DataSantriTsnModel;
        $dataSearch = $dataSantri->where('nama_lengkap','LIKE','%'.$keyword.'%')->get();
        
        return $dataSearch;
    }

    public function getSantriTsnById(Request $request){
        $id = $request->input("id");
        $dataSantri = new DataSantriTsnModel;
        $data = $dataSantri->where('id', $id);

        return $data;
    }

}