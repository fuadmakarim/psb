<?php
namespace App\Modules\Admin\Controllers;
use App\Http\Controllers\Controller;
class RegisterAdminController extends Controller
{
    public function registerAdmin(){
        $role = "admin_mln";
        return view('auth.register', array(
            'role' => $role
        ));
    }
}