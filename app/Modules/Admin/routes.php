<?php
Route::group(['namespace' => 'App\Modules\Admin\Controllers', 'middleware' => 'web', 'prefix' => '/admin'], function() {

    //Home
    Route::get('/', 'AdminMlnController@index');
    Route::get('/pendaftaran/add/{id}', 'AdminMlnController@add');
    Route::post('/pendaftaran/create', 'AdminMlnController@create');
    Route::get('/pembayaran', 'AdminMlnController@indexPembayaran');
    Route::get('/pembayaran/validasi/{id}/{pendaftarId}', 'AdminMlnController@validasiPembayaran');
    Route::get('/getSantriTsn', 'AdminMlnController@getSantriTsn');
    Route::get('/getSantriTsnById', 'AdminMlnController@getSantriTsnById');
    Route::get('/pendaftar-online', 'AdminMlnController@indexPendaftarOnline');
    Route::get('/pendaftar-final', 'AdminMlnController@indexPendaftarFinal');
    Route::get('/psbadminreg', 'RegisterAdminController@registerAdmin');
    Route::get('/dashboard', 'AdminMlnController@dashboard');
 });