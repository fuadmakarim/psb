<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css')}}" >
    <link rel="stylesheet" type="text/css" href="{{ asset('public/js/datatable/datatables.min.css')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type=text/javascript>var app_url={!!json_encode(url('/'))!!}</script>
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

    <title>ADMIN PSB</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-success">
  <a class="navbar-brand" href="#">ADMIN PSB</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item"><a href="{{url('/admin/dashboard')}}" class="nav-link">Beranda</a></li>
    <li class="nav-item"><a href="{{url('/admin')}}" class="nav-link">Daftar Santri</a></li>
    <li class="nav-item"><a href="{{url('/admin/pembayaran')}}" class="nav-link">Pembayaran</a></li>
    <li class="nav-item"><a href="{{url('/admin/pendaftar-online')}}" class="nav-link">Pendaftar Online</a></li>
    <li class="nav-item"><a href="{{url('/admin/pendaftar-final')}}" class="nav-link">Pendaftar Final</a></li>

    </ul>
    <ul class="navbar-nav">
       @if (Auth::guest())
            <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Buat Akun</a></li>
        @else
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{ Auth::user()->name }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                            Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
            </li> 
        @endif
    </ul>
  </div>
</nav>
    @yield('content')
   
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('public/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('public/js/popper.min.js')}}"></script>
    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
    <script>
        $('.to-santri-panel').click(function(e){
            e.preventDefault();
            $('a[href="#santri-panel"]').tab('show');
        });
        $('.to-ortu-panel').click(function(e){
            e.preventDefault();
            $('a[href="#ortu-panel"]').tab('show');
        });
        $('.to-alamat-panel').click(function(e){
            e.preventDefault();
            $('a[href="#alamat-panel"]').tab('show');
        });
        $('.to-jurusan-panel').click(function(e){
            e.preventDefault();
            $('a[href="#jurusan-panel"]').tab('show');
        });
        $('.to-pembayaran-panel').click(function(e){
            e.preventDefault();
            $('a[href="#pembayaran-panel"]').tab('show');
        });
        $('#backDetailBtn').click(function(e){
            e.preventDefault();
            $("#detail").css("display", "none");
        });
        $('.show-detail-btn').click(function(e){
            e.preventDefault();
            $("#detail").css("display", "block");
        });
        $('body').click(function(event){
        // check if the clicked element is a descendent of navigation 
        if ($(event.target).closest('#optionSantri').length) {
            return; //do nothing if event target is within the navigation
        } else {
            $('#optionSantri').removeClass('show');
        // do something if the event target is outside the navigation
            // code for collapsing menu here...
        }
        });
    </script>
    <script src="{{asset('public/js/address.js') }}"></script>
    <script src="{{asset('public/js/jurusan.js') }}"></script>
    <script src="{{asset('public/js/datasantri.js') }}"></script>
    
    <script type="text/javascript" src="{{asset('public/js/datatable/datatables.min.js')}}"></script>
    <script>
        $(document).ready( function () {
            $('#tablePendaftar').DataTable({
                buttons: [
                    'excel'
                ]
            });
        } );    
    </script>
  </body>
</html>