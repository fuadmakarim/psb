<?php

namespace App\Modules\Auth\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function __construct()
    {
       Auth::logout();
    }

    public function index(){
    	return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $no_hp = $request->input('no_hp');
        $password = $request->input('password');
        if (Auth::attempt(['no_hp' => $no_hp, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('/');
        }
    }
}
