<?php

namespace App\Modules\User\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Laravolt\Indonesia\Facade;
use App\Http\Controllers\Controller;
use App\Modules\Registrasi\Models\PendaftarMlnModel;
use App\Modules\Registrasi\Models\PembayaranMlnModel;
use App\Modules\Registrasi\Models\StatusRegistrasiModel;
class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $pendaftarMln = new PendaftarMlnModel;
        $userId = Auth::id();
        $dataCalonSantri = $pendaftarMln->where('user_id', $userId)->first();

        $statusRegistrasi = new StatusRegistrasiModel;
        $dataStatus = $statusRegistrasi->where('user_id', $userId)->first();

        $pembayaran = new PembayaranMlnModel;
        $statusPembayaran = $pembayaran->where('user_id', $userId)->first();
        
        // dd($userId);
        $provinces = $this->getProvinces();
        // dd($userId);
        return view('User::DashboardView', array(
            'dataCalonSantri' => $dataCalonSantri,
            'dataStatus' => $dataStatus,
            'provinces' => $provinces,
            'statusPembayaran' => $statusPembayaran,
        ));
    }
    public function getProvinces(){
        return Facade::allProvinces();
    }
    public function getCities($provinceId){
        $data = Facade::findProvince($provinceId, 'cities');
        $cities = $data->cities;
        return $cities;
    }
    public function getDistricts($cityId){
        $data = Facade::findCity($cityId, 'districts');
        $districts = $data->districts;
        return $districts;
    }
    public function getVillages($districtId){
        $data = Facade::findDistrict($districtId, 'villages'); 
        $villages = $data->villages;
        return $villages;
    }

    public function cetakKartu($id){
        $pendaftarMln = new PendaftarMlnModel;
        $dataCalonSantri = $pendaftarMln->where('id', $id)->first();
        $date = date('d/m/Y');
        return view('Registrasi::Mln.KartuPesertaView', array(
            'dataCalonSantri' => $dataCalonSantri,
            'date' => $date
        ));
    }

    public function cetakKwitansi($id){
        $pendaftarMln = new PendaftarMlnModel;
        $dataCalonSantri = $pendaftarMln->where('id', $id)->first();
        $date = date('d/m/Y');
        return view('Registrasi::Mln.KwitansiPesertaView', array(
            'dataCalonSantri' => $dataCalonSantri,
            'date' => $date
        ));
    }

    // public function getProvinces() {
    //     $district = Facade::allProvinces();
    //     return $district;
    // }

    // public function getDistrict($id) {
    //     $district = Facade::findProvince($provinceId, 'cities');
    //     return json_encode($district);
    // }

    // public function getSubDistrict($id) {
    //     $sub_district = Facade::findCity($cityId, 'districts');
    //     return json_encode($sub_district);
    // }
}