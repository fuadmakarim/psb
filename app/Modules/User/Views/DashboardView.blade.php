@extends('Layouts::layout')
@section('content')
@include('User::DetailView')
<div class="container-fluid">
<nav class="mt-3">
    <div class="nav nav-pills nav-fill" id="nav-tab" role="tablist">
        @if ($dataStatus)
            @php
                $dataSantri = $dataStatus->status_data_santri;
                $dataOrtu = $dataStatus->status_data_ortu;
                $dataAlamat = $dataStatus->status_data_alamat;
                $dataJurusan = $dataStatus->status_data_jurusan;
                $dataPembayaran = $dataStatus->status_data_pembayaran;
            @endphp
        @else
            @php
                $dataSantri = 0;
                $dataOrtu = 0;                    
                $dataAlamat = 0;
                $dataJurusan = 0;
                $dataPembayaran = 0;
            @endphp
        @endif
        <a class="nav-item nav-link active" 
            id="nav-staus-tab" 
            data-toggle="tab" 
            href="#status-panel" 
            role="tab" 
            aria-controls="nav-status" 
            aria-selected="true">
            Status Pendaftaran
        </a>
        <a class="nav-item nav-link" 
            id="nav-santri-tab" 
            data-toggle="tab" 
            href="#santri-panel" 
            role="tab" 
            aria-controls="nav-santri" 
            aria-selected="false">
            Data Calon Santri
        </a>
        <a class="nav-item nav-link {{$dataSantri == 1 ? "" : "disabled"}}"
            id="nav-ortu-tab" 
            data-toggle="tab" 
            href="#ortu-panel" 
            role="tab" 
            aria-controls="nav-ortu" 
            aria-selected="false">
            Data Orang Tua Santri<br>
            <span class="small text-warning">{{$dataSantri == 1 ? "" : "Data santri belum lengkap"}}</span>
        </a>
        {{-- <a class="nav-item nav-link" 
            id="nav-alamat-tab" 
            data-toggle="tab" 
            href="#alamat-panel" 
            role="tab" 
            aria-controls="nav-alamat" 
            aria-selected="false">
            Alamat
        </a> --}}
        <a class="nav-item nav-link {{$dataSantri == 1 ? "" : "disabled"}}"
            id="nav-jurusan-tab" 
            data-toggle="tab" 
            href="#jurusan-panel" 
            role="tab" 
            aria-controls="nav-jurusan" 
            aria-selected="false">
            Pilihan Peminatan/Jurusan<br>
            <span class="small text-warning">{{$dataSantri == 1 ? "" : "Data santri belum lengkap"}}</span>
        </a>
        <a class="nav-item nav-link 
            {{$dataSantri == 1 &&
                $dataOrtu == 1 &&
                $dataJurusan == 1 
                ? "" : "disabled"}}"
            id="nav-pembayaran-tab" 
            data-toggle="tab" 
            href="#pembayaran-panel" 
            role="tab" 
            aria-controls="nav-pembayaran" 
            aria-selected="false">
            Pembayaran<br>
            <span class="small text-warning">{{$dataSantri == 1 ? "" : "Data santri belum lengkap"}}</span>
        </a>
        {{-- <a class="nav-item nav-link" 
            id="nav-password-tab" 
            data-toggle="tab" 
            href="#password-panel" 
            role="tab" 
            aria-controls="nav-password" 
            aria-selected="false">
            Ubah Password
        </a> --}}
    </div>
</nav>
</div>
    <hr>
<div class="container">      
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="status-panel" role="tabpanel" aria-labelledby="nav-status-tab">
            <br>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($dataStatus)
                @php
                    $dataSantri = $dataStatus->status_data_santri;
                    $dataOrtu = $dataStatus->status_data_ortu;
                    $dataAlamat = $dataStatus->status_data_alamat;
                    $dataJurusan = $dataStatus->status_data_jurusan;
                    $dataPembayaran = $dataStatus->status_data_pembayaran;
                @endphp
            @else
                @php
                    $dataSantri = 0;
                    $dataOrtu = 0;                    
                    $dataAlamat = 0;
                    $dataJurusan = 0;
                    $dataPembayaran = 0;
                @endphp
            @endif
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div>Data Calon Santri<br>   
                            @if($dataSantri == 1) 
                                <span class="small"><a href="" class="show-detail-btn">Lihat</a> | <a  href="#" class="to-santri-panel">Edit</a></span> 
                            @else
                                <a  href="#" class="to-santri-panel">Lengkapi sekarang</a>
                            @endif
                            </div>
                            <span class="badge {{$dataSantri == 1 ? 'badge-success' : 'badge-warning'}} badge-pill">
                                {{$dataSantri == 1 ? 'Sukses' : 'Belum lengkap'}}
                            </span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div>Data Orang Tua Santri<br>
                            @if($dataOrtu == 1) 
                                <span class="small"><a href="" class="show-detail-btn">Lihat</a> | <a href="#" class="to-ortu-panel">Edit</a></span> 
                            @else
                                <a href="#" class="to-ortu-panel">Lengkapi sekarang</a>
                            @endif
                            </div>
                            <span class="badge {{$dataOrtu == 1 ? 'badge-success' : 'badge-warning'}} badge-pill">
                                {{$dataOrtu == 1 ? 'Sukses' : 'Belum lengkap'}}
                            </span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div>Pilihan Peminatan / Jurusan<br>
                            @if($dataJurusan == 1) 
                                <span class="small"><a href="" class="show-detail-btn">Lihat</a> | <a  href="#" class="to-jurusan-panel">Edit</a></span> 
                            @else
                                <a href="#" class="to-jurusan-panel">Lengkapi sekarang</a>
                            @endif
                            </div>
                            <span class="badge {{$dataJurusan == 1 ? 'badge-success' : 'badge-warning'}} badge-pill">
                                {{$dataJurusan == 1 ? 'Sukses' : 'Belum lengkap'}}
                            </span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div>Pembayaran<br>
                            @if($dataPembayaran == 1) 
                                <span class="small"><span  href="#" class="to-pembayaran-panel text-success">Konfirmasi pembayaran diterima</a></span> 
                            @else
                                <a href="#" class="to-pembayaran-panel">Bayar sekarang</a>
                            @endif
                            </div>
                            <span class="badge {{$dataPembayaran == 1 ? 'badge-success' : 'badge-warning'}} badge-pill">
                                {{$dataPembayaran == 1 ? 'Menunggu Validasi' : 'Belum dibayar'}}
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    @if($statusPembayaran)
                        @php
                            $statusBayar = $statusPembayaran->status;
                       @endphp
                    @else
                        @php
                            $statusBayar = 0;    
                        @endphp
                    @endif
                    @if($dataCalonSantri)
                        @php
                            $noTes = $dataCalonSantri->no_tes;
                            $noRuang = $dataCalonSantri->no_ruang;    
                        @endphp
                    @else
                        @php
                            $noTes = 0;
                            $noRuang = 0;
                        @endphp
                    @endif
                    <div class="card {{$statusBayar === "Dibayar" ? 'bg-success text-white' : 'bg-light'}} mb-3">
                    <div class="card-header">Status Pendaftaran 
                        <span class="float-right">
                            <b>{{$statusBayar === "Dibayar" ? 'AKTIF' : 'BELUM AKTIF'}}</b>
                        </span></div>
                        <div class="card-body">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>No Ujian</td>
                                        <td>:</td>
                                        <td><b>{{$noTes === 0 ? '-' : $noTes}}</b></td>
                                    </tr>
                                    <tr>
                                        <td>No Ruangan</td>
                                        <td>:</td>
                                        <td><b>{{$noRuang === 0 ? '-' : $noRuang}}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <i>Status pendaftaran akan menjadi Aktif setelah seluruh data diisi dan pembayaran telah diterima</i>
                            @if($noTes)
                                <a href="{{url('cetak-kartu/'.$dataCalonSantri->id)}}" class="btn btn-light btn-block">Download Kartu Peserta</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="santri-panel" role="tabpanel" aria-labelledby="nav-santri-tab">
            @include('Registrasi::Mln.DataSantriView')
        </div>
        <div class="tab-pane fade" id="ortu-panel" role="tabpanel" aria-labelledby="nav-ortu-tab">
            @include('Registrasi::Mln.DataOrangTuaView')
        </div>
        {{-- <div class="tab-pane fade" id="alamat-panel" role="tabpanel" aria-labelledby="nav-alamat-tab">
            @include('Registrasi::Mln.AlamatView')
        </div> --}}
        <div class="tab-pane fade" id="jurusan-panel" role="tabpanel" aria-labelledby="nav-jurusan-tab">
            @include('Registrasi::Mln.PilihanJurusanView')
        </div>
        <div class="tab-pane fade" id="pembayaran-panel" role="tabpanel" aria-labelledby="nav-pembayaran-tab">
            @include('Registrasi::Mln.PembayaranView')
        </div>
        {{-- <div class="tab-pane fade" id="password-panel" role="tabpanel" aria-labelledby="nav-password-tab">
            @include('Registrasi::Mln.UbahPasswordView')
        </div> --}}
    </div>
</div>
 <br><br><br>
@endsection