<div id="detail" class="position-fixed w-100 h-100 bg-white p-3 rounded-top" style="z-index:100; overflow:auto; display:none">
    <a href="" id="backDetailBtn">
    <ion-icon class="d-inline-block align-middle" name="arrow-back"></ion-icon> 
    <span class="d-inline-block align-middle"> Kembali</span>
    </a>
    <div class="col-md-8 offset-md-2">
        @if($dataCalonSantri)
        <h5>Data Santri</h5>
        
        <table class="table">
            <tbody>
                <tr>
                    <td scope="row" width="30%">Nama Lengkap</td>
                    <td width="5%">:</td>
                    <td>{{$dataCalonSantri->nama_lengkap}}</td>
                </tr>
                <tr><td>No Tes</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->no_tes}} </td>
                </tr>
                <tr><td>No Ruang</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->no_ruang}} </td>
                </tr>
                <tr><td>NISN</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->nisn}} </td>
                </tr>
                <tr><td>Tahun Lulus</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->tahun_lulus}} </td>
                </tr>
                <tr><td>Tempat & Tanggal Lahir</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->tempat_lahir}} , {{$dataCalonSantri->tanggal_lahir}} </td>
                </tr>
                <tr><td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->jenis_kelamin}} </td>
                </tr>
                <tr><td>Anak ke</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->anak_ke}} dari {{$dataCalonSantri->dari_bersaudara}} bersaudara</td>
                </tr>
                <tr><td>Nama Sekolah Asal</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->sekolah_asal}} </td>
                </tr>
                <tr><td>Alamat Sekolah Asal</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->alamat_sekolah_asal}} </td>
                </tr>
                <tr><td>Kategori Sekolah Asal</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->kategori_sekolah_asal}}</td>
                </tr>
                <tr><td>Rencana tinggal saat belajar</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->rencana_tinggal}} </td>
                </tr>
                <tr><td>Dorongan masuk pesantren</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->dorongan_masuk}} </td>
                </tr>
                <tr><td>Sumber informasi PSB</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->sumber_informasi}} </td>
                </tr>
            </tbody>
        </table>
        <br>
        <h5>Data Orangtua Santri</h5>
        <table class="table">
            <tbody>
                <tr>
                    <td scope="row" width="30%">Nama Ayah</td>
                    <td width="5%">:</td>
                    <td>{{$dataCalonSantri->nama_ayah}}</td>
                </tr>
                <tr><td>Pendidikan Ayah</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->pendidikan_ayah}} </td>
                </tr>
                <tr><td>Pekerjaan Ayah</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->pekerjaan_ayah}} </td>
                </tr>
                <tr>
                    <td scope="row" width="30%">Nama Ibu</td>
                    <td width="5%">:</td>
                    <td>{{$dataCalonSantri->nama_ibu}}</td>
                </tr>
                <tr><td>Pendidikan Ibu</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->pendidikan_ibu}} </td>
                </tr>
                <tr><td>Pekerjaan Ibu</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->pekerjaan_ibu}} </td>
                </tr>
                <tr>
                    <td scope="row" width="30%">Nama Wali</td>
                    <td width="5%">:</td>
                    <td>{{$dataCalonSantri->nama_wali}}</td>
                </tr>
                <tr><td>Pendidikan Wali</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->pendidikan_wali}} </td>
                </tr>
                <tr><td>Pekerjaan Wali</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->pekerjaan_wali}} </td>
                </tr>
                <tr>
                    <td scope="row" width="30%">Hubungan Wali dengan Anak</td>
                    <td width="5%">:</td>
                    <td>{{$dataCalonSantri->hubungan_wali_anak}}</td>
                </tr>
                <tr><td>Penghasilan Orang Tua</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->penghasilan_orangtua}} </td>
                </tr>
                <tr><td>Penanggung Biaya Pendidikan</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->penanggung_biaya}} </td>
                </tr>
                <tr>
                    <td scope="row" width="30%">Alamat</td>
                    <td width="5%">:</td>
                    <td>{{$dataCalonSantri->alamat_lengkap}}</td>
                </tr>
                <tr><td>Provinsi</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->provinsi}} </td>
                </tr>
                <tr><td>Kabupaten/Kota</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->kabupaten}} </td>
                </tr>
                <tr>
                    <td scope="row" width="30%">Kecamatan</td>
                    <td width="5%">:</td>
                    <td>{{$dataCalonSantri->kecamatan}}</td>
                </tr>
                <tr><td>Kelurahan/Desa</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->kelurahan}} </td>
                </tr>
                <tr><td>No HP 1</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->no_hp_1}} </td>
                </tr>
                <tr><td>No HP 2</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->no_hp_2}} </td>
                </tr>
              
            </tbody>
        </table>
        <br>
        <h5>Pilihan Jurusan</h5>
           
        <table class="table">
            <tbody>
                <tr>
                    <td scope="row" width="30%">Pilihan Jurusan 1</td>
                    <td width="5%">:</td>
                    <td>{{$dataCalonSantri->pilihan_jurusan_1}}</td>
                </tr>
                <tr><td>Pilihan Jurusan 2</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->pilihan_jurusan_2}} </td>
                </tr>
                <tr><td>Pilihan Jurusan 3</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->pilihan_jurusan_3}} </td>
                </tr>
            </tbody>
        </table>
        <br><br><br>
        @endif
    </div>
</div>