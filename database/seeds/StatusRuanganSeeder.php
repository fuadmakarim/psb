<?php

use Illuminate\Database\Seeder;

class StatusRuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_ruangan')->insert([
            'id' => 1,
            'jumlah_peserta' => 0,
            'no_ruang' => 1,
        ]);
    }
}
