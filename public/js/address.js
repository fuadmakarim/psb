$(document).ready(function() {
  $('select[name="provinceId"]').on("change", function() {
    var stateID = $(this)
      .find(":selected")
      .data("id");
    $('select[name="cityId"]').append(
      "<option value=''><i>Loading...</i></option>"
    );
    $('select[name="districtId"]').empty();
    if (stateID) {
      $.ajax({
        url: app_url + "/getCities/" + stateID,
        type: "GET",
        dataType: "json",
        success: function(data) {
          $('select[name="cityId"]').empty();
          $('select[name="cityId"]').append(
            "<option ><i>silahkan pilih kabupaten</i></option>"
          );
          $.each(data, function(key, value) {
            $('select[name="cityId"]').append(
              '<option data-id=["' +
                value.id +
                '"] value="' +
                value.name +
                '">' +
                value.name +
                "</option>"
            );
          });
        }
      });
    } else {
      $('select[name="citytId"]').empty();
    }
  });

  $('select[name="cityId"]').on("change", function() {
    var stateID = $(this)
      .find(":selected")
      .data("id");
    $('select[name="districtId"]').append(
      "<option value=''><i>Loading...</i></option>"
    );
    $('select[name="villageId"]').empty();
    if (stateID) {
      $.ajax({
        url: app_url + "/getDistricts/" + stateID,
        type: "GET",
        dataType: "json",
        success: function(data) {
          $('select[name="districtId"]').empty();
          $('select[name="districtId"]').append(
            "<option><i>silahkan pilih kecamatan</i></option>"
          );
          $.each(data, function(key, value) {
            $('select[name="districtId"]').append(
              '<option  data-id=["' +
                value.id +
                '"] value="' +
                value.name +
                '">' +
                value.name +
                "</option>"
            );
          });
        }
      });
    } else {
      $('select[name="districtId"]').empty();
    }
  });

  $('select[name="districtId"]').on("change", function() {
    $('select[name="villageId"]').append(
      "<option value=''><i>Loading...</i></option>"
    );

    var stateID = $(this)
      .find(":selected")
      .data("id");
    if (stateID) {
      $.ajax({
        url: app_url + "/getVillages/" + stateID,
        type: "GET",
        dataType: "json",
        success: function(data) {
          $('select[name="villageId"]').empty();
          $('select[name="villageId"]').append(
            "<option ><i>silahkan pilih desa</i></option>"
          );
          $.each(data, function(key, value) {
            $('select[name="villageId"]').append(
              '<option  data-id=["' +
                value.id +
                '"]  value="' +
                value.name +
                '">' +
                value.name +
                "</option>"
            );
          });
        }
      });
    } else {
      $('select[name="villageId"]').empty();
    }
  });
});
